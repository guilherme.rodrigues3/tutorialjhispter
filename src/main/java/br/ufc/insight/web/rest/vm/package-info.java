/**
 * View Models used by Spring MVC REST controllers.
 */
package br.ufc.insight.web.rest.vm;
