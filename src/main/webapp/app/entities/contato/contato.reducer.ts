import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IContato, defaultValue } from 'app/shared/model/contato.model';

export const ACTION_TYPES = {
  FETCH_CONTATO_LIST: 'contato/FETCH_CONTATO_LIST',
  FETCH_CONTATO: 'contato/FETCH_CONTATO',
  CREATE_CONTATO: 'contato/CREATE_CONTATO',
  UPDATE_CONTATO: 'contato/UPDATE_CONTATO',
  DELETE_CONTATO: 'contato/DELETE_CONTATO',
  RESET: 'contato/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IContato>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ContatoState = Readonly<typeof initialState>;

// Reducer

export default (state: ContatoState = initialState, action): ContatoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CONTATO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CONTATO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CONTATO):
    case REQUEST(ACTION_TYPES.UPDATE_CONTATO):
    case REQUEST(ACTION_TYPES.DELETE_CONTATO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CONTATO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CONTATO):
    case FAILURE(ACTION_TYPES.CREATE_CONTATO):
    case FAILURE(ACTION_TYPES.UPDATE_CONTATO):
    case FAILURE(ACTION_TYPES.DELETE_CONTATO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTATO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTATO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CONTATO):
    case SUCCESS(ACTION_TYPES.UPDATE_CONTATO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CONTATO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/contatoes';

// Actions

export const getEntities: ICrudGetAllAction<IContato> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CONTATO_LIST,
  payload: axios.get<IContato>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IContato> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CONTATO,
    payload: axios.get<IContato>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IContato> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CONTATO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IContato> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CONTATO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IContato> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CONTATO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
