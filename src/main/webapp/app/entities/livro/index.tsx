import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Livro from './livro';
import LivroDetail from './livro-detail';
import LivroUpdate from './livro-update';
import LivroDeleteDialog from './livro-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LivroUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LivroUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LivroDetail} />
      <ErrorBoundaryRoute path={match.url} component={Livro} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LivroDeleteDialog} />
  </>
);

export default Routes;
