import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './livro.reducer';
import { ILivro } from 'app/shared/model/livro.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILivroDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LivroDetail = (props: ILivroDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { livroEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="tutorialJhApp.livro.detail.title">Livro</Translate> [<b>{livroEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="nome">
              <Translate contentKey="tutorialJhApp.livro.nome">Nome</Translate>
            </span>
          </dt>
          <dd>{livroEntity.nome}</dd>
          <dt>
            <span id="codigo">
              <Translate contentKey="tutorialJhApp.livro.codigo">Codigo</Translate>
            </span>
          </dt>
          <dd>{livroEntity.codigo}</dd>
          <dt>
            <span id="autor">
              <Translate contentKey="tutorialJhApp.livro.autor">Autor</Translate>
            </span>
          </dt>
          <dd>{livroEntity.autor}</dd>
        </dl>
        <Button tag={Link} to="/livro" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/livro/${livroEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ livro }: IRootState) => ({
  livroEntity: livro.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LivroDetail);
