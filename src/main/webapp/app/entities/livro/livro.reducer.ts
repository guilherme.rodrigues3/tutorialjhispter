import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILivro, defaultValue } from 'app/shared/model/livro.model';

export const ACTION_TYPES = {
  FETCH_LIVRO_LIST: 'livro/FETCH_LIVRO_LIST',
  FETCH_LIVRO: 'livro/FETCH_LIVRO',
  CREATE_LIVRO: 'livro/CREATE_LIVRO',
  UPDATE_LIVRO: 'livro/UPDATE_LIVRO',
  DELETE_LIVRO: 'livro/DELETE_LIVRO',
  RESET: 'livro/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILivro>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type LivroState = Readonly<typeof initialState>;

// Reducer

export default (state: LivroState = initialState, action): LivroState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LIVRO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LIVRO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LIVRO):
    case REQUEST(ACTION_TYPES.UPDATE_LIVRO):
    case REQUEST(ACTION_TYPES.DELETE_LIVRO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LIVRO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LIVRO):
    case FAILURE(ACTION_TYPES.CREATE_LIVRO):
    case FAILURE(ACTION_TYPES.UPDATE_LIVRO):
    case FAILURE(ACTION_TYPES.DELETE_LIVRO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LIVRO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LIVRO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LIVRO):
    case SUCCESS(ACTION_TYPES.UPDATE_LIVRO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LIVRO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/livros';

// Actions

export const getEntities: ICrudGetAllAction<ILivro> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LIVRO_LIST,
  payload: axios.get<ILivro>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ILivro> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LIVRO,
    payload: axios.get<ILivro>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILivro> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LIVRO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILivro> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LIVRO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILivro> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LIVRO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
