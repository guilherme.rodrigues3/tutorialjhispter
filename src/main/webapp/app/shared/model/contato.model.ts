export interface IContato {
  id?: number;
  nome?: string;
  email?: string;
  telefone?: string;
}

export const defaultValue: Readonly<IContato> = {};
