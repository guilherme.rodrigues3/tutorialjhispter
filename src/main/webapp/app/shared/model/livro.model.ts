export interface ILivro {
  id?: number;
  nome?: string;
  codigo?: string;
  autor?: string;
}

export const defaultValue: Readonly<ILivro> = {};
